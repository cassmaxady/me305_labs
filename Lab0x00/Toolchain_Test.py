def perm_gen_lex(my_str):
    """Takes a string and returns all possible permutations in lexicographic order"""
    if len(my_str) == 0:                               # Checks for empty input
        return []
    if len(my_str) == 1:                               # Base case
        return [my_str]
    else:
        perm_list = []
        for idx in range(len(my_str)):                 # Loops once for every character found in the input string
            start = my_str[idx]
            str_new = my_str[:idx] + my_str[idx + 1:]  # Makes new string without the character found at the index
            perm = perm_gen_lex(str_new)
            for char in perm:                          # Loops to place all smaller permutation lists back into big list
                perm_list.append(start + char)
        return perm_list

print(perm_gen_lex('abc'))