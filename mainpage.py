'''!@file                mainpage.py
    @brief               Creates the main page for the portfolio using Doxygen documentation

    @mainpage

    @section sec_intro   Introduction
                         My name is Maxwell Cassady, this is my ME 305 Intro to Mechatronics Portfolio. This portfolio
                         documents and demonstrates all the code written throughout this quarter, culminating in the
                         final project, the ball balancing platform. To access each different lab and homework page,
                         please look below and clock the desired link.

    @section sec_Lab1    Lab 1
                         Created a program to change the output of an LED light when a button is pressed
                         \n See https://lmurray03.bitbucket.io/_page1.html for complete description

    @section sec_Lab2    Lab 2
                         Wrote code to read and update the position of an encoder, being able to gather positional
                         data as well as perform other simple functions.
                         \n See https://cassmaxady.bitbucket.io/_page2.html for complete description

    @section sec_HW2     HW 0x02
                         To model the ball balancing system, we analyzed the linkages as a 2D model, creating a set
                         of matrices that fully describe the system response.
                         \n See https://cassmaxady.bitbucket.io/_page3.html for the complete analysis

    @section sec_Lab3    Lab 3
                         Wrote a program to run both motors on our board simultaneously and added their functionality
                         to the primary taskUser function created in lab 2.
                         \n See https://cassmaxady.bitbucket.io/_page4.html for the complete description

    @section sec_HW3     HW 0x03
                         To complete the model of the ball balancing system, we simulated the system response using
                         MATLAB to see how it would react under specific conditions.
                         \n See https://cassmaxady.bitbucket.io/_page5.html for the complete analysis

    @section sec_Lab4    Lab 4
                         Wrote a program that will use closed loop control that will continuously update the motor
                         velocity while trying to reach a set velocity using an error feedback loop.
                         \n See https://cassmaxady.bitbucket.io/_page6.html for the complete description

    @section sec_Lab5    Lab 5
                         Created a class and task that can read usable data from an IMU and can calibrate or
                         continuously update along with other tasks.
                         \n See https://cassmaxady.bitbucket.io/_page7.html for the complete description

    @section sec_Term    Lab 0xFF: Term Project
                         The goal of this project was to code a platform with an MCU, IMU, and a resistive touch panel
                         that could balance a ball on top for an extended period of time. The project utilizes closed
                         loop control to balance the platform and ball actively using the data from the IMU and touch
                         panel.
                         \n See https://cassmaxady.bitbucket.io/_page8.html for the complete description

    @section sec_srcd    Source Code
                         To directly access the source code for this portfolio, please click the link below. The source
                         code for each file is also provided at the bottom of its respective page on this website.
                         \n https://bitbucket.org/cassmaxady/me305_labs/src/master/

    @author              Maxwell Cassady

    @date                March 18, 2022

    @page Page1          Lab 0x01

    @section Lab1_title  Lab0x01 Getting Started with Hardware

    @section sec_desc    LED Button
                         The program is for an STM NUCLEO L476 microcontroller to recognize the button
                         input at pin C13 and use it to create an interrupt request that switches the
                         LED light output at pin A5.
                         \n See the file description here: https://cassmaxady.bitbucket.io/_m_e305___lab0x01_8py.html

    @author              Maxwell Cassady

    @date                March 18, 2022

    @page Page2          Lab 0x02

    @section Lab2_title  Lab 0x02 Incremental Encoders

    @section sec_enc     Encoder
                         The encoder files shown below work in tandem to read and update the position of an encoder,
                         being able to gather positional data as well as perform other simple functions.

    @subsection sec_plot Sample Plot
                         Here is an example of the encoder position plotted as a function of time gathered by the
                         g or G function in taskUser.

                         @image html encoderPlot.png width=500px

    @subsection sec_encDr Encoder Driver
                         The encoder driver initiates an encoder on the microcontroller that is able to read output
                         from the encoder on the motor.
                         \n Please see https://cassmaxady.bitbucket.io/encoder_8py.html for details.

    @subsection sec_encTE Encoder Task
                         The encoder task performs the encoder driver manipulation to return the desired outputs back
                         to the user interface.
                         \n Please see https://cassmaxady.bitbucket.io/task_encoder_8py.html for details.

    @subsection sec_encTU User Task
                         The user task allows for interface with a user, waiting for commands to then send them off
                         to the relevant task file to be performed.
                         \n Please see https://cassmaxady.bitbucket.io/task_user_8py.html for details.

    @subsection sec_main Main
                         Main allows for the interface between the encoder, motor, and user tasks so that they can work
                         in tandem at the same frequency with the same shared variables.
                         \n Please see https://cassmaxady.bitbucket.io/main_8py.html for details.

    @subsection sec_shr  Shares
                         The share file instantiates the classes share and queue which allow for the transferral
                         and sharing of data between different files.
                         \n Please see https://cassmaxady.bitbucket.io/shares_8py.html for details.

    @author              Maxwell Cassady

    @date                March 18, 2022

    @page Page3          HW 0x02

    @section Page3_title HW0x02 Ball Balancer System Modeling

    @section sec_desc    System Modeling
                         Below is the handwritten work to derive the equations that model our ball balancer system.
                         We began by finding the kinematics of the system, separating our analysis into looking
                         at the ball and platform and just the ball by itself.
                         @image html HW2_1.png width=750px
                         @image html HW2_2.png width=750px
                         @image html HW2_3.png width=750px

                         After finding the kinematics, we moved on to find the kinetics of the system using
                         free body diagrams and kinetic diagrams and setting the moments about the base
                         of the platform equal to each other. After that we arranged our solution into
                         a matrix format so tha we will be able to easily model it later on in HW0x03
                         @image html HW2_4.png width=750px
                         @image html HW2_5.png width=750px
                         @image html HW2_6.png width=750px

    @author              Maxwell Cassady

    @date                March 18, 2022

    @page Page4          Lab 0x03

    @section Lab3_title  Lab 0x03 PMDC Motors

    @section sec_mot     Motor
                         The motor and driver files listed below work together to instantiate two motors and a driver
                         and link them with taskUser from lab 0x02 so that they can be used from the user interface.
                         This lab added considerable functionality to the user interface created in lab 0x02, allowing
                         for the interaction and testing of the motors on our board.

    @subsection sec_plot Sample Plots
                         Here is an example of the encoder position plotted as a function of time gathered by the
                         g or G function in taskUser. This plot is similar to the one found in lab 0x02, but it is now
                         updated to show the position in radians.
                         @image html encoderPlotRad.png width=500px

                         Here is an example of the results found from testing the encoder velocities at different duty
                         cycles using the t or T function in taskUser. It is plotted along with the theoretical
                         velocities that we would expect at those same duty cycles for a motor that can run at 1800RPM,
                         validating our data.
                         @image html testerPlot.png width=500px

    @subsection sec_motC Motor Object
                         The motor file instantiates a motor object inside the MCU. This motor object is able to control
                         either motor on the board, changing the duty cycles.
                         \n Please see https://cassmaxady.bitbucket.io/motor_8py.html for details.

    @subsection sec_drC  Driver Object
                         The driver file instantiates a driver object on the MCU. The driver object can instantiate two
                         motors and control each of them individually, as well as allowing for the checking of faults.
                         It also allows for the enabling and disabling of the motors.
                         \n Please see https://cassmaxady.bitbucket.io/driver_8py.html for details.

    @subsection sec_motT Motor Task
                         The motor task performs the necessary manipulation of an instantiated driver with two motors
                         needed to change the duty cycle of each motor independently and to reset faults by reenabling
                         the motors.
                         \n Please see https://cassmaxady.bitbucket.io/task_motor_8py.html for details.

    @subsection sec_aux  Edited Lab 0x02 Files
                         While creating the new lab 0x03 files, we added to and edited the following files from lab
                         0x02, changing the code and remaking the state-transition and task diagrams:
                         \n   -taskUser
                         \n   -main
                         \n Please check Lab 0x02 at https://cassmaxady.bitbucket.io/_page2.html for the file details.

    @author              Maxwell Cassady

    @date                March 18, 2022

    @page Page5          HW 0x03

    @section Page5_title HW0x03 Ball Balancer Simulation

    @section sec_desc2   System Modeling Simulation
                        ----------UNDER CONSTRUCTION---------

    @author              Maxwell Cassady

    @date                March 18, 2022

    @page Page6          Lab 0x04

    @section Lab4_title  Lab 0x04 Closed Loop Speed Control

    @section sec_clc     Closed Loop Controller
                         The closed loop controller made for this lab works by instantiating an object in the motor
                         task and using the motor task to update and run the closed loop control with the same timing
                         as the other tasks. Our closed loop controller only works with a proportional gain as of now,
                         but we plan on adding in functionality for an integral control gain later on. Adding in closed
                         loop control required the addition of a few states in the user task to allow the user to
                         interface with the new capabilities of the microcontroller.

    @subsection sec_pltT Tuning Plots
                         For every test while tuning our closed loop controller, we kept the reference velocity at
                         100rad/s to get consistent results. We initially tested our closed loop control response at a
                         gain og 0.8, and we got violently oscillating behavior in the motor as seen below.
                         @image html propGainPlot1.PNG width=500px

                         To reduce this large oscillating response, we reduced the proportional gain down to 0.2 which
                         did successfully reduce the large oscillations, but it led to very jerky behavior.
                         @image html propGainPlot2.PNG width=500px

                         We then increased the gain to 0.6 to see if we could get a non-jerky response with less
                         oscillation, and 0.6 successfully gave us that while reaching about 50% of our desired
                         motor velocity at steady state.
                         @image html propGainPlot3.PNG width=500px

                         To see if we could improve the response at all, we dropped the gain down to 0.5, which reached
                         steady state quicker with less extreme transient oscillation, but it settled at a slightly
                         lower velocity of around 45% of the reference.
                         @image html propGainPlot4.PNG width=500px

                         From our plots, it seems that depending on the constraints our system was under, we could
                         choose between the gains of 0.5 and 0.6, depending on whether more overshoot was okay if the
                         reference velocity is closer like we see with the gain of 0.6, or if overshoot is something we
                         are trying to avoid in which case 0.5 would be a better choice. We see the massive error
                         between our reference velocity and actual velocity because proportional gain control is not
                         particularly accurate on its own, and when we add in integral control later on, we will see
                         far better responses from the motor.

    @subsection sec_pltB Block Diagram
                         Below is a block diagram representation of our system, showing how the closed loop feedback is
                         sent back into the motor driver to change the duty cycle, which then in turn feeds back in,
                         hence the name closed loop control.
                         @image html Lab4BlockDiagram.png width=1000px

    @subsection sec_clco Closed Loop Control Object
                         The closed loop control file instantiates a closed loop controller object inside the MCU. This
                         closed loop controller object allows the usage of a proportional gain with a set reference
                         velocity to constantly update the duty cycle of the motor to reach that speed.
                         \n Please see https://cassmaxady.bitbucket.io/closed_loop_control_8py.html for details.

    @subsection sec_auc Edited Lab 0x02 and Lab 0x03 Files
                         While creating the new lab 0x04 files, we added to and edited the following files from lab
                         0x02 and lab 0x03, changing the code and remaking the state-transition and task diagrams:
                         \n   -    taskUser
                         \n (https://cassmaxady.bitbucket.io/task_user_8py.html)
                         \n      Added the closed loop controller functionality required for lab 4, adding a few states
                         \n      and expanding our command and printing states.
                         \n   -    main
                         \n (https://cassmaxady.bitbucket.io/main_8py.html)
                         \n      Added a few shared variables needed to use the set gains and reference velocities
                         \n      across the motor task and user task.
                         \n   -    taskMotor
                         \n (https://cassmaxady.bitbucket.io/task_motor_8py.html)
                         \n      Added the closed loop controller functionality into taskMotor to constantly update the
                         \n      duty cycle when the closed loop control is activated and to avoid creating a new task

    @page Page7          Lab 0x05

    @section Lab5_title  Lab 0x05 I2C and Inertial Measurement Units

    @section sec_descI   IMU To MCU Over I2C
                         This lab focused on creating an IMU driver class that could connect and provide feedback to
                         our microcontroller through an I2C serial communication bus from a BNO055 IMU. The IMU task
                         instantiates an I2C object and then runs in tandem with the user task to constantly update
                         the euler angles and angular velocities of the BNO055 IMU on the platform. These angles and
                         velocities can then be accessed from other tasks for use in closed loop control to balance
                         the board and the ball on the board.

    @subsection sec_I2C  I2C Object
                         The I2C file instantiates an I2C serial communication port between the microcontroller and
                         a BNO055 IMU device. It allows for the calibration of the device and the reading of euler
                         angles and angular velocities.
                         \n Please see https://cassmaxady.bitbucket.io/_i2_c_8py.html for details.

    @subsection sec_IMUt IMU Task
                         The IMU task performs the necessary manipulation of the I2C object, keeping it updated along
                         with the other tasks and following user interface commands.
                         \n Please see https://cassmaxady.bitbucket.io/task_i_m_u_8py.html for details.

    @subsection sec_aucI Edited Files From Previous Labs
                         At this point in the quarter, our focus changed from working on our mechatronics abilities
                         to building towards our term project: the ball balancing platform. Because of this, much of
                         the previous functionality of the user interface was removed as it was no longer relevant for
                         the platform balancing, and the encoder class and task were removed entirely, being replaced
                         by the IMU task and class. To show all of our previous progress while still being able to
                         build towards the term project, we renamed many of the still relevant files that were used
                         for earlier labs so that if it was desired, you could still go back and run the old code
                         with its completely different functions and abilities. The files used in this lab require the
                         term project files to show their functionality, so please see the term project page linked
                         below.
                         \n   -    https://cassmaxady.bitbucket.io/_page8.html

    @author              Maxwell Cassady

    @date                March 18, 2022

    @page Page8          Lab 0xFF

    @section Term_title  Lab 0xFF Ball Balancer Term Project

    @section sec_descBB  Ball Balancing Platform Summary
                         This project is the culmination of all the previous labs, using a little bit from every
                         one. The goal of this project was to get a platform to actively balance a ball using an
                         outer loop controller that sent reference position data to an inner loop controller which
                         could then balance a ball by controlling the motor duty cycles of the motors on the x and y
                         axes. Unfortunately, we were unable to tune our ball balancing platform to a point at which it
                         could successfully balance the ball, but we got the platform to respond correctly to the
                         position of the ball on the platform. The platform also successfully balanced itself
                         when there was no contact with the touch panel.

    @subsection sec_ft   Balancing Platform Design Overview
                         Our ball balancer runs using cooperative multitasking with time slicing to switch between the
                         five task files that we use. The first file, taskUser, essentially runs as a command task,
                         running all of the user interface and reading all the user input commands. This task then
                         dictates what to run in the other tasks accordingly. The next task, taskMotor, controls the
                         motor duty cycles and runs the closed loop control on the platform's x- and y-axis motors. It
                         is able to change the gain coefficients and toggle the closed loop control on and off, also
                         knowing when to run both outer and inner loop control or just inner loop depending on
                         whether there is contact with the resistive touch panel. The third task, taskIMU, allows for
                         the connection of the board microcontroller with an IMU placed on the platform, which will
                         then read the angle and angular velocity data from the IMU for use by the rest of the tasks.
                         Similarly, taskPanel connects a resistive touch panel with the microcontroller, which then
                         reads the x and y positions and velocities of the ball on the panel for use in the closed
                         loop control. Finally, taskData is in charge of reading this data and writing it to files
                         when told to by taskUser.
                         \n\n
                         Follow these links to see the full documentation of each of these tasks.
                         \n   -    taskUser_F
                         \n (https://cassmaxady.bitbucket.io/task_user___f_8py.html)
                         \n   -    taskMotor_F
                         \n (https://cassmaxady.bitbucket.io/task_motor___f_8py.html)
                         \n   -    taskIMU
                         \n (https://cassmaxady.bitbucket.io/task_i_m_u_8py.html)
                         \n   -    taskPanel
                         \n (https://cassmaxady.bitbucket.io/task_panel_8py.html)
                         \n   -    taskData
                         \n (https://cassmaxady.bitbucket.io/task_data_8py.html)
                         \n\n
                         Here is the complete task diagram of how each of the tasks interacts with each other.
                         @image html TermTaskDiagram.PNG width=1000px
                         \n\n
                         One of the cooler features of the platform is that when calibrating the touch panel, the panel
                         will respond with a small amount of haptic feedback when done calibrating at each position.
                         The rest of our ball balancing platform features can be seen below in the sample puTTy windows
                         and in the video.

    @subsection sec_bbct Balancing Platform Control Overview
                         Our ball balancer functions using an inner and outer loop controller. The outer loop
                         controller takes the reference position of the ball on the platform (always equal to zero) and
                         subtracts the actual position of the ball, multiplying it by the proportional gain to make the
                         proportional controller. It then goes through a derivative gain to finally output the reference
                         angular position needed to return the ball to zero. This reference angle goes through a
                         saturation limit, keeping the absolute value of the max angle under 10 so that the ball doesn't
                         go flying. This filtered angle then goes through the inner loop controller with similar
                         proportional and derivative control, outputting the motor duty needed to return the ball back
                         to the center of the platform. This motor duty output also goes through a saturation limit,
                         causing the max motor duty coming from the controller to be less than the 40 or greater than
                         -40. Integral gain is not included in the control since it was causing issues with the
                         balancing.

    @subsection sec_bbpt Example PuTTy Windows
                         To demonstrate the full functionality of our user interface, the PuTTy windows after each
                         input is shown here.

                         \n\n @image html PTY1.png width=500px
                         This image shows the full user interface along with each of the possible commands that a user
                         can put in. Below the command panel, after pressing the keys "p" and "v", the PuTTy window
                         will show the euler angles and angular velocities of the platform's current orientation. At
                         the time these commands were called, the platform was balanced and not moving, hence the
                         close to 0 values for each.

                         \n\n @image html PTY2.png width=500px
                         This image shows the PuTTy screen after changing the gain coefficients for both the inner and
                         outer loops. As you can see, our integral gains are deactivated for this ball balancing
                         project because our platform balanced the ball better without it.

                         \n\n @image html PTY3.png width=500px
                         This image shows the PuTTy screen after both the IMU and touch panel calibration commands are
                         called. The IMU was already calibrated, so it shows that the calibration file was read from,
                         but the touch panel still needed to be calibrated, so it shows the process of how that is done.

                         \n\n @image html PTY4.png width=500px
                         This image shows the PuTTy screen after collecting a full 20 seconds of data and after
                         stopping the data midway through collection. They are fairly similar, stopping the collection
                         early merely prints out a stopping message.

                         \n\n @image html PTY5.png width=500px
                         This image shows the PuTTy screen after using the show current ball position function. As you
                         can see, the function does nothing until there is contact with the board and then it will
                         print out where it is in millimeters away from the origin. It then turns off again when
                         contact with the platform ceases.

    @subsection sec_tch  Resistive Touch Panel Object
                         The touch file instantiates a resistive touch panel that can measure the position and velocity
                         of a ball along its surface. It also allows for the calibration of the device to ensure the
                         platform is reading corrected position and velocity values.
                         \n @image html PTYTEST.png width=500px
                         To verify that our three scans could run sequentially in under 1500 microseconds, we ran all
                         three 1000 times and averaged the time it took. Our three scans took around 1050 microseconds
                         to run each time, staying below the requirement.
                         \n\n Please see https://cassmaxady.bitbucket.io/touch_8py.html for more details on the touch
                         class.

    @subsection sec_vid  Video Demonstration
                         Please click the link below to see a video demonstration of how our ball balancing platform
                         works
                         \n https://drive.google.com/file/d/1KllZbFO3XsMIUkkP-je4vUoZPuBxSWUV/view?usp=sharing

    @author              Maxwell Cassady

    @date                March 18, 2022
'''