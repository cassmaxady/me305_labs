'''!@file                   ME305_Lab0x01.py
    @brief                  Program to change the output of an LED light when a button is pressed
    @details                The program is created for an STM NUCLEO L476 microcontroller to
                            recognize the button input at pin C13 and use it to create an
                            interrupt request that switches the LED light output at pin A5.
                            @image html LED_FSM.png
                            @image html Button_FSM.png
                            See a video of the code output on the microcontroller at
                            https://drive.google.com/file/d/15uv9jhiehYf324MMxv1OPe8x68T7BTZ5/view?usp=sharing
                            Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x01/ME322%20Lab0x01.py
    @author                 Max Cassady
    @author                 Lucas Murray
    @date                   (1/18/2022)
'''

import math
import time
import pyb


def onButtonPressFCN(IRQ_src):
    '''! Recognizes the external input of the button being pressed
       @details Takes the interrupt request passed by the ButtonInt variable
                when the button is pressed and changes the buttonPressed value
                to True
       @param IRQ_src    interrupt request from source
    '''
    ## @brief Stores the boolean variable for the button input
    #
    global buttonPressed
    buttonPressed = True


## @brief Allocates the pin located at C13 on the MCU to the variable C13
#
C13 = pyb.Pin(pyb.Pin.cpu.C13)
## @brief Allocates the pin located at A5 on the MCU to the variable A5
#
A5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
## @brief Makes a callback to C13 when an external input is recieved
#
ButtonInt = pyb.ExtInt(C13, mode=pyb.ExtInt.IRQ_FALLING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)

## @brief Creates a timer object using timer 2 with a trigger at 2000Hz
#
tim2 = pyb.Timer(2, freq=2000)
## @brief Initiates a channel object for pin A5
#
t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=A5)


def Square(elapsedTime):
    '''! Calculates and returns the values for a square wave
    @param elapsedTime    Time elapsed since the button was last pressed
    @return    The y-value of the square wave with respect to time
    '''
    return round(elapsedTime % 1)


def Sine(elapsedTime):
    '''! Calculates and returns the values for a sine wave
    @param elapsedTime    Time elapsed since the button was last pressed
    @return    The y-value of the sine wave with respect to time
    '''
    return math.sin(elapsedTime * 2 * math.pi / 10)


def Saw(elapsedTime):
    '''! Calculates and returns the values for a saw wave
    @param elapsedTime    Time elapsed since the button was last pressed
    @return    The y-value of the saw wave with respect to time
    '''
    return elapsedTime % 1


if __name__ == '__main__':
    ## @brief Exists as a placeholder for the state number and allows it change
    #
    count = 0
    buttonPressed = False
    print('Behold a blinking light')
    print('Press the blue button to cycle the LED pattern')

    while True:
        try:

            if count == 0:
                time.sleep(1)
                print('Waiting...')
                if buttonPressed:
                    buttonPressed = False
                    ## @brief Records the time at which the state changes
                    #
                    start_time = time.ticks_ms()
                    print('Square Wave')
                    count = 1

            elif count == 1:  # Square Wave

                ## @brief Continuously records and replaces the current time
                #
                current_time = time.ticks_ms()
                ## @brief Records the difference between current and start time
                #
                waveTime = time.ticks_diff(current_time, start_time) / 1000
                ## @brief Constantly records and replaces the brightness value
                #
                brightness = Square(waveTime) * 100
                t2ch1.pulse_width_percent(brightness)

                if buttonPressed:
                    buttonPressed = False
                    start_time = time.ticks_ms()
                    print('Sine Wave')
                    count = 2

            elif count == 2:  # Sine Wave

                current_time = time.ticks_ms()
                waveTime = time.ticks_diff(current_time, start_time) / 1000
                brightness = (Sine(waveTime) / 2 + 0.5) * 100
                t2ch1.pulse_width_percent(brightness)

                if buttonPressed:
                    buttonPressed = False
                    start_time = time.ticks_ms()
                    print('Saw Wave')
                    count = 3

            elif count == 3:  # Saw Wave

                current_time = time.ticks_ms()
                waveTime = time.ticks_diff(current_time, start_time) / 1000
                brightness = Saw(waveTime) * 100
                t2ch1.pulse_width_percent(brightness)

                if buttonPressed:
                    buttonPressed = False
                    start_time = time.ticks_ms()
                    print('Square Wave')
                    count = 1

        except KeyboardInterrupt:
            break

    print("Project Terminated")