"""!@file       closedLoopControl_Term.py
    @brief      A driver to provide a feedback loop for closed loop control.
    @details    The closed loop class instantiates a closed loop controller that will calculate the error of the
                current position and velocity and compare to a reference speed and velocity, updating the motor 
                speed based on the error calculated. The closed loop controller can calculate the error using 
                proportional gain, derivative gain, and integral gain, although for the term project the integral 
                gain is commented out due to it interfering with the platform balancing.

                \nThis is the second closed loop control class that can be found on this portfolio. It was changed so
                much for the term project that it would no longer work for the Lab0x04 functionality, so we split them
                up to maintain the functionality of our old code as well as the term project.

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Term%20Project/closedLoopControl_Term.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       March 17, 2022
"""

class ClosedLoop:
    '''!@brief A closed loop feedback object that calculates the error in the output.
        @details Objects of this class can find the error in the output using by comparing the current position and
                 velocity to a reference position and velocity by using the equations for proportional, derivative, and
                 integral control. The integral control is currently commented out due to it interfering with our ball
                 balancing in the term project.
    '''

    def __init__(self, refPos, refVel, Kp, Kd, Ki, Vang, Eang):
        '''!@brief Initializes and returns a closed loop object.
            @param refPos The reference position
            @param refVel The reference velocity
            @param Kp     The proportional gain coefficient
            @param Kd     The derivative gain coefficient
            @param Ki     The integral gain coefficient
            @param Vang   A shared tuple that contains the angular velocities from the IMU
            @param Eang   A shared tuple that contains the euler angles from the IMU
        '''
        # Initiating relevant position and gain values
        self.refPosX = refPos
        self.refPosY = refPos
        self.refVel = refVel
        self.propGain = Kp
        self.dervGain = Kd
        self.inteGain = Ki
        
        # Initiating the angular velocities shared from taskMotor
        vX, vY, vZ = Vang
        self.actualVelX = vX
        self.actualVelY = vY
        
        # Initiating the euler angles shared from taskMotor
        x, y, z = Eang
        self.actualPosX = x
        self.actualPosY = y
        
        # Starts the integral controls at 0
        self.inteConX = 0
        self.inteConY = 0
    
    def update(self, Vang, Eang):
        '''!@brief Updates the feedback error found by the closed loop control.
            @param Vang   A shared tuple that contains the angular velocities from the IMU
            @param Eang   A shared tuple that contains the euler angles from the IMU
        '''
        
        # -----------------------X-Motor Control-----------------------
        
        propConX = (-1 * self.propGain * (self.refPosX - self.actualPosX))
        dervConX = (-1 * self.dervGain * (self.refVel - self.actualVelX))
        '''
        self.refPosXi = -1.5*self.actualPosX
        if self.refPosXi > -1.5 and self.refPosXi < 1.5:
            self.refPosXi = 0
        self.inteConX += (-1 * self.inteGain * (self.refPosXi - self.actualPosX)) / 100
        '''
        self.motorDutyX = propConX + dervConX #+ self.inteConX
        
        # Duty Cycle saturation limit
        dlim = 40
        if self.motorDutyX > dlim:
            self.motorDutyX = dlim
        if self.motorDutyX < -dlim:
            self.motorDutyX = -dlim
        
        # -----------------------Y-Motor Control-----------------------

        propConY = ( 2 * self.propGain * (self.refPosY - self.actualPosY))
        dervConY = ( 1.2 * self.dervGain * (self.refVel - self.actualVelY))
        '''
        self.refPosYi = -1.5 * self.actualPosY
        if self.refPosYi > -1.5 and self.refPosYi < 1.5:
            self.refPosYi = 0
        self.inteConY += ( 1 * self.inteGain * (self.refPosYi - self.actualPosY)) / 100
        '''
        self.motorDutyY = propConY + dervConY #+ self.inteConY
        
        # Duty Cycle saturation limit
        if self.motorDutyY > dlim:
            self.motorDutyY = dlim
        if self.motorDutyY < -dlim:
            self.motorDutyY = -dlim
        
        # -----------------------CLOOP Feedback------------------------
        self.actualVelX, self.actualVelY, null = Vang.read()
        self.actualPosX, self.actualPosY, null = Eang.read()
    
    def setGains(self, Kp, Kd, Ki):
        '''!@brief A function to set the gain values for the proportional, derivative, and integral control.
        '''
        self.propGain = Kp
        self.dervGain = Kd
        self.inteGain = Ki
        
    def setRefPos(self, refPosX, refPosY):
        '''!@brief A function to set the reference positions for the controller
        '''
        self.refPosX = refPosX
        self.refPosY = refPosY
        
        
