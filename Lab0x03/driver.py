"""!@file       driver.py
    @brief      A driver that controls the two motors on the board.
    @details    The driver class instantiates a driver that can control the behavior of the motors connected to it.
                It is able to detect faults in the motor as well as able to disable and re-enable both motors at once.
                It can create multiple motors internally which allows the use of the motor class functions through the
                driver.

                \n Source Code: https://bitbucket.org/lmurray03/me305_lucas_murray/src/master/Lab%200x03/driver.py

    @author     Lucas Murray
    @author     Max Cassady
    @date       February 16, 2022
"""


from motor import Motor
from pyb import Timer, Pin, ExtInt
import time

class DRV8847:
    '''!@brief  A motor driver class for the DRV8847 driver from TI.
        @details Objects of this class can be used to configure the DRV8847
                motor driver and to create one or more objects of the
                Motor class which can be used to perform motor
                control.
    
    Refer to the DRV8847 datasheet here:
    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    def __init__ (self, timNum):
        '''!@brief Initializes and returns a driver object.
            @param timNum the timer number for the pins being used
        '''
        self.timPWM = Timer(timNum, freq = 20_000)
        self.nSleep = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
        self.nSleep.low()
        
        self.nFault = Pin(Pin.cpu.B2)
        self.nFault.low()
        
        self.faultInt = ExtInt(self.nFault, mode=ExtInt.IRQ_FALLING, pull=Pin.PULL_NONE, callback=self.faultCB)
        
    
    def enable (self):
        '''!@brief Brings the driver out of sleep mode.
            @details To bring the driver out of sleep mode without causing a fault, the fault pin is disabled for 50
            microseconds while it turns on
        '''
        
        self.faultInt.disable()
        self.nSleep.high()
        time.sleep(50/1000000)
        self.faultInt.enable()
        
    
    def disable (self):
        '''!@brief Puts the driver in sleep mode.
        '''
        self.nSleep.low()
    
    
    def faultCB (self, IRQ_src):
        '''!@brief Callback function to run on fault condition.
            @param IRQ_src The source of the interrupt request.
        '''
        self.disable()
        print('Fault detected, stopping motor')
        
    
    def motor (self, pinIn1, pinIn2, chA, chB):
        '''!@brief Creates a DC motor object connected to the driver.
            @param pinIn1 The first pin associated with the motor
            @param pinIn2 The second pin associated with the motor
            @param chA    The first channel associated with the motor
            @param chB    The second channel associated with the motor
            @return An object of class Motor
        '''
        motor = Motor(self.timPWM, pinIn1, pinIn2, chA, chB)
        return motor
    
    
if __name__ == '__main__':
    # Driver test code
    
     # Motor 1 pins
    pinB4 = Pin(Pin.cpu.B4, Pin.OUT_PP)
    pinB5 = Pin(Pin.cpu.B5, Pin.OUT_PP)
    
    # Motor 2 pins
    pinB0 = Pin(Pin.cpu.B0, Pin.OUT_PP)
    pinB1 = Pin(Pin.cpu.B1, Pin.OUT_PP)
    
    motorDRV = DRV8847(3)
    motor1 = motorDRV.motor(pinB4, pinB5, 1, 2)
    motor2 = motorDRV.motor(pinB0, pinB1, 3, 4)
    
    # Enable the motor driver
    motorDRV.enable()
    
    # Set the duty cycle of the first motor to 40 percent and the duty cycle of
    # the second motor to 60 percent
    motor1.setDuty(40)
    motor2.setDuty(25)