# -*- coding: utf-8 -*-
"""!@file   taskUser.py
@brief
@details
@author     Lucas Murray
@author     Max Cassady
@date       January 31, 2022
"""

from time import ticks_us, ticks_add, ticks_diff, ticks_ms
from pyb import USB_VCP
import micropython
from encoder import Encoder
import array

S0_INIT = micropython.const(0)
S1_CMD = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_PRINT = micropython.const(3)
S4_DELTA = micropython.const(4)
S5_COLLECT = micropython.const(5)
S6_END = micropython.const(6)

def taskUserFcn(taskName, period, zFlag, pFlag, dFlag, gFlag, sFlag, data):
    '''!@brief              A generator to implement the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name, interval and zFlag status to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Boolean variable shared between taskUser and
                            taskEncoder for the zero command
        @param pFlag        Boolean variable shared between taskUser and
                            taskEncoder for the print position command
        @param dFlag        Boolean variable shared between taskUser and
                            taskEncoder for the print delta command
        @param gFlag        Boolean variable shared between taskUser and
                            taskEncoder for the collect data command
        @param sFlag        Boolean variable shared between taskUser and
                            taskEncoder for the end data collection command
    '''
   
    state = S0_INIT
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    ser = USB_VCP()
    posArray = array.array('l', 3000*[0])
    timArray = array.array('l', 3000*[0])
    numPrint = 0
    
    while True:
       
        current_time = ticks_us()
       
        if ticks_diff(current_time, next_time) >= 0:
            next_time = ticks_add(next_time, period)
           
            if state == S0_INIT:
                printUI()
                state = S1_CMD
           
           
            elif state == S1_CMD:
                if ser.any():
                    charIn = ser.read(1).decode()
                    if charIn in {'z', 'Z'}:
                        print('Zeroing encoder... ')
                        zFlag.write(True)
                        state = S2_ZERO
                    elif charIn in {'p', 'P'}:
                        print('POSITION')
                        pFlag.write(True)
                        state = S3_PRINT
                    elif charIn in {'d', 'D'}:
                        print('DELTA')
                        dFlag.write(True)
                        state = S4_DELTA
                    elif charIn in {'g', 'G'}:
                        print('COLLECT')
                        gFlag.write(True)
                        state = S5_COLLECT
                    elif charIn in {'s', 'S'}:
                        print('ENDING COLLECTION')
                        sFlag.write(True)
                        state = S6_END
                        
                    else:
                        raise ValueError('Press a better key')
                   
            elif state == S2_ZERO:
                if not zFlag.read():
                    print('Encoder zeroed')
                    zFlag.write(False)
                    state = S1_CMD
                    
            elif state == S3_PRINT:
                if not pFlag.read():
                    print('Position printed')
                    pFlag.write(False)
                    state = S1_CMD
                    
            elif state == S4_DELTA:
                if not dFlag.read():
                    print('Delta printed')
                    dFlag.write(False)
                    state = S1_CMD
                    
            elif state == S5_COLLECT:
                if not gFlag.read():
                    numItem = 0
                    while numItem < 3000:
                        [timArray[numItem], posArray[numItem]] = data.read()
                        print(timArray[numItem], posArray[numItem])
                        print(numItem)
                        numItem += 1
                        if ser.any():
                            charIn = ser.read(1).decode()
                            if charIn in {'s', 'S'}:
                                gFlag.write(False)
                                state = S6_END
                    print('Data collected')
                    gFlag.write(False)
                    state = S6_END
                    
            elif state == S6_END:
                #if not sFlag.read():
                numPrint = 0
                if numPrint < numItem:
                    print(f'{((timArray[numPrint]-timArray[0])/1000):.2f}, {(posArray[numPrint]):.2f}')
                    numPrint += 1
                print('Data collection ended')
                #sFlag.write(False)
                state = S1_CMD
                   
            else:
                raise ValueError('Press z')
       
            next_time = ticks_add(next_time, period)
       
            yield state
       
       
        else:
            yield None
       
       
def printUI():
    print('+--------------------------------------+\n'
          '|ME 305 Position Encoder User Interface|\n'
          '+--------------------------------------+\n'
          '|              |Commands|              |\n'
          '|z or Z - Zero the Encoder             |\n'
          '|p or P - Print Position               |\n'
          '|d or D - Print Position Change        |\n'
          '|g or G - Collect and Print Encoder    |\n'
          '|         Position for 30 Seconds      |\n'
          '|s or S - End Data Collection Early    |\n'
          '+--------------------------------------+')
    
    
'''
if __name__ == '__main__':
    # A generator object created from the generator function for the user
    # interface task.
    task1 = taskUserFcn('T1', 10_000, False)
    
    # To facilitate running of multiple tasks it makes sense to create a task
    # list that can be iterated through. Adding more task objects to the list
    # will allow the tasks to run together cooperatively.
    taskList = [task1]
    
    # The system should run indefinitely until the user interrupts program flow
    # by using a Ctrl-C to trigger a keyboard interrupt. The tasks are run by
    # using the next() function on each item in the task list.
    while True:
        try:
            for task in taskList:
                next(task)
        
        except KeyboardInterrupt:
            break
            
    # In addition to exiting the program and informing the user any cleanup code
    # should go here. An example might be un-configuring certain hardware
    # peripherals like the User LED or perhaps any callback functions.
    print('Program Terminating')            
'''