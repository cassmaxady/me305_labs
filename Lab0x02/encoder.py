# -*- coding: utf-8 -*-
'''!@file   encoder.py
@brief      A driver for reading from Quadrature Encoders
@details    The Encoder class initiates an object that can read the position
            of a quadrature encoder. It allows the user to update and print
            the position and the change in position from the previous update
            at the desired time intervals as well as zero the encoder.
@author     Lucas Murray
@author     Max Cassady
@date       January 31, 2022
'''

import pyb
import time

class Encoder:
    '''!@brief  Interface with quadrature encoders
    @details    Can read and print the position of a quadrature encoder as
                well as control the update speed for the position.

    '''

    def __init__(self, pinA, pinB, timNum):
        '''!@brief      Constructs an encoder object
        @param pinA     The first pin associated with the encoder
        @param pinB     The second pin associated with the encoder
        @param timNum   The number of the timer that works for the given pins
        '''
        print('Creating encoder object')
        self.tim4 = pyb.Timer(timNum, prescaler=0, period=0xFFFF)
        self.tim4.channel(1, pyb.Timer.ENC_AB, pin=pinA)
        self.tim4.channel(2, pyb.Timer.ENC_AB, pin=pinB)
        self.position = 0

    def update(self, updatePosition):
        '''!@brief  Updates encoder position and delta
        @details    Takes the position of the encoder and recalculates it,
                    updating the change in position in the process
        @param updatePosition The current position of the encoder
        @return [update_time, updatePosition] The time and position of the encoder after the update is finished
        '''
        #update_time = time.ticks_ms()
       
        self.currentPosition = self.tim4.counter()
        self.delta = self.currentPosition - updatePosition
        #self.position = self.get_position()
        #self.delta = self.position - updatePosition
        
        if self.delta > 32768:
            self.delta -= 65536
        elif self.delta < -32768:
            self.delta += 65536
        
        self.position += self.get_delta()
        
        updatePosition = self.currentPosition
        return [updatePosition]
       
        

    def get_position(self):
        '''!@brief Returns the encoder position
        @return The position of the encoder shaft
        '''
        #print('Getting position')
        return self.position

    def zero(self):
        '''!@brief Resets the encoder position to zero
        '''
        print('Setting position back to zero')
        self.position = 0

    def get_delta(self):
        '''!@brief Returns change in position from previous update
        @return The change in position of the encoder shaft
        between the two most recent updates
        '''
        return self.delta

if __name__ == '__main__':
   
    B6 = pyb.Pin(pyb.Pin.cpu.B6)
    B7 = pyb.Pin(pyb.Pin.cpu.B7)
   
    myEncoder = Encoder(B6, B7, 4)
    start_time = time.ticks_ms()
    #update_time=0
    updatePosition=0
    while True:
       
        try:
            current_time = time.ticks_ms()
            elapsedTime = time.ticks_diff(current_time, start_time) / 10
            if elapsedTime % 1 == 0:
                [updatePosition] = myEncoder.update(updatePosition)
                print([myEncoder.get_position(), myEncoder.get_delta()])
           
        except KeyboardInterrupt:
            print('cancel')
            break