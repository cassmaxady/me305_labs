# -*- coding: utf-8 -*-
"""!@file   taskEncoder.py
@brief
@details
@author     Lucas Murray
@author     Max Cassady
@date       January 31, 2022
"""

import pyb
from time import ticks_us, ticks_add, ticks_diff, ticks_ms
import micropython
from encoder import Encoder
import time, array


S0_INIT = micropython.const(0)
S1_UPDATE = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_PRINT = micropython.const(3)
S4_DELTA = micropython.const(4)
S5_COLLECT = micropython.const(5)
S6_END = micropython.const(6)

def taskEncoderFcn(taskName, period, zFlag, pFlag, dFlag, gFlag, sFlag, data):
    '''!@brief              Performs the tasks as determined by the taskUser file
        @details            Interfaces with the taskUser file through the use of
                            the shared flag parameters, keeping the encoder
                            updated and performing the input commands
        @param taskName     The name of the task a string
        @param period       The period of the task in microseconds as an integer
        @param zFlag        Boolean variable shared between taskUser and
                            taskEncoder for the zero command
        @param pFlag        Boolean variable shared between taskUser and
                            taskEncoder for the print position command
        @param dFlag        Boolean variable shared between taskUser and
                            taskEncoder for the print delta command
        @param gFlag        Boolean variable shared between taskUser and
                            taskEncoder for the collect data command
        @param sFlag        Boolean variable shared between taskUser and
                            taskEncoder for the end data collection command
    '''

    B6 = pyb.Pin(pyb.Pin.cpu.B6)
    B7 = pyb.Pin(pyb.Pin.cpu.B7)
    myEncoder = Encoder(B6, B7, 4)

    state = S0_INIT
    next_time = ticks_add(ticks_us(), period)
    start_time = ticks_us()
    updatePosition = 0

    while True:
        
        current_time = ticks_us()
        
        if ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                state = S1_UPDATE
                
            elif state == S1_UPDATE:
                #myEncoder.update(updatePosition)
                
                update_time = time.ticks_us()
                elapsedTime = time.ticks_diff(update_time, start_time)
                data.write((elapsedTime, myEncoder.get_position()))
                
                #if elapsedTime % 1 == 0:
                [updatePosition] = myEncoder.update(updatePosition)
                
                if zFlag.read():
                    state = S2_ZERO
                elif pFlag.read():
                    state = S3_PRINT
                elif dFlag.read():
                    state = S4_DELTA
                elif gFlag.read():
                    state = S5_COLLECT
                elif sFlag.read():
                    state = S6_END
                    
                
            elif state == S2_ZERO:
                myEncoder.zero()
                zFlag.write(False)
                state = S1_UPDATE
                
            elif state == S3_PRINT:
                print(myEncoder.get_position())
                pFlag.write(False)
                state = S1_UPDATE
                
            elif state == S4_DELTA:
                print(myEncoder.get_delta())
                dFlag.write(False)
                state = S1_UPDATE
                    
            elif state == S5_COLLECT:
                #myArray = array.array([])
                
                gFlag.write(False)
                state = S1_UPDATE
                
            elif state == S6_END:
                print('boo2')
                sFlag.write(False)
                state = S1_UPDATE
            
            else:
                raise ValueError(f'Invalid State in {taskName}')
                
            next_time = ticks_add(next_time, period)
            
            yield state
            
        else:
            
            yield None


        '''
        start_time = current_time
        numItem = 0
        while elapsedTime < 300:
            current_time = time.ticks_ms()
            elapsedTime = time.ticks_diff(current_time, start_time) / 10
            if elapsedTime % 10 == 0:
                while numItem < 3000:
                        
                            #myArray.append([elapsedTime, myEncoder.get_position()])
                    posArray.append(myEncoder.get_position())
                    timArray.append(int(numItem))
                    numItem += 1
        collectList = (timArray, posArray)
        print(collectList)
        '''
                #goes in state 5