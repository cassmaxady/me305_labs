# -*- coding: utf-8 -*-
'''!@file   main.py
@brief
@details
@author     Lucas Murray
@author     Max Cassady
@date       January 31, 2022
'''


import time
from encoder import Encoder
from taskEncoder import taskEncoderFcn
from taskUser import taskUserFcn
import shares

zFlag = shares.Share()
pFlag = shares.Share()
dFlag = shares.Share()
gFlag = shares.Share()
sFlag = shares.Share()
data = shares.Share((0, 0))

if __name__ == '__main__':

    # A generator object created from the generator function for the user
    # interface task.
    # task1 = taskUserFcn('T1', 10_000, True)

    # To facilitate running of multiple tasks it makes sense to create a task
    # list that can be iterated through. Adding more task objects to the list
    # will allow the tasks to run together cooperatively.
    #taskList = [task1]
    taskList = [taskEncoderFcn('taskEncoder', 10_000, zFlag, pFlag, dFlag, gFlag, sFlag, data),
                taskUserFcn('taskUser', 10_000, zFlag, pFlag, dFlag, gFlag, sFlag, data)]

    # The system should run indefinitely until the user interrupts program flow
    # by using a Ctrl-C to trigger a keyboard interrupt. The tasks are run by
    # using the next() function on each item in the task list.
    while True:
        try:
            for task in taskList:
                next(task)

        except KeyboardInterrupt:
            break

    # In addition to exiting the program and informing the user any cleanup code
    # should go here. An example might be un-configuring certain hardware
    # peripherals like the User LED or perhaps any callback functions.
    print('Program Terminating')